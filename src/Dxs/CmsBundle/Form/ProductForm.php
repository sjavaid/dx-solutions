<?php

// src/Dxs/CmsBundle/Form/ProductForm.php

namespace Dxs\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductForm extends AbstractType {

    private $id;

    public function __construct($id) {
        $this->id = $id;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        if ($this->id > 0) {

            $builder->add('id', 'text', array(
                'label' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'readonly' => 'readonly'
                )
                    )
            );
        }

        $builder
                ->add('products', 'text', array(
                    'label' => false,
                    'max_length' => 100,
                    'attr' => array(
                        'class' => 'span6',
                        'autocomplete' => 'off'
                    )
                        )
                )
                ->add('price', 'text', array(
                    'label' => false,
                    'max_length' => 10,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                    )
                        )
                )
                ->add('categoryid', 'entity', array(
                    'label' => false,
                    'class' => 'DxsCmsBundle:CategoryEntity',
                    'property' => 'categories',
                    'empty_value' => 'Select an option',
                    'empty_data' => '',
                    'attr' => array(
                        'class' => 'chzn-select'
                    )
                        )
                )
                ->add('details', 'textarea', array(
                    'label' => false,
                    'attr' => array(
                        'class' => 'span6',
                        'cols' => '10',
                        'rows' => '5'
                    )
                        )
                )
                ->add('slug', 'text', array(
                    'label' => false,
                    'max_length' => 100,
                    'attr' => array(
                        'class' => 'span6',
                        'autocomplete' => 'off'
                    )
                        )
                )
                ->add('active', 'checkbox', array(
                    'label' => false,
                    'required' => false
                        )
                )
                ->add('save', 'submit', array(
                    'attr' => array(
                        'formnovalidate' => 'formnovalidate',
                        'class' => 'btn btn-primary',
                        'label' => 'Save Changes'
                    )
                        )
                )
                ->getForm();
    }

    public function getName() {
        return 'products';
    }
}
