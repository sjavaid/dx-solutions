<?php

// src/Dxs/CmsBundle/Form/CategoryForm.php

namespace Dxs\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryForm extends AbstractType {

    private $id;

    public function __construct($id) {
        $this->id = $id;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        if ($this->id > 0) {

            $builder->add('id', 'text', array(
                'label' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'readonly' => 'readonly'
                )
                    )
            );
        }

        $builder
                ->add('categories', 'text', array(
                    'label' => false,
                    'max_length' => 100,
                    'attr' => array(
                        'class' => 'span6',
                        'autocomplete' => 'off'
                    )
                        )
                )
                ->add('slug', 'text', array(
                    'label' => false,
                    'max_length' => 255,
                    'attr' => array(
                        'class' => 'span6',
                        'autocomplete' => 'off'
                    )
                        )
                )
                ->add('save', 'submit', array(
                    'attr' => array(
                        'formnovalidate' => 'formnovalidate',
                        'class' => 'btn btn-primary',
                        'label' => 'Save Changes'
                    )
                        )
                )
                ->getForm();
    }

    public function getName() {
        return 'categories';
    }

}
