<?php
// src/Dxs/CmsBundle/Custom/StringClass.php

namespace Dxs\CmsBundle\Custom;

/**
 * Description of StringClass
 *
 * @author Salman
 */
class StringClass {

    public function generateSlug($phrase) {

        $result = strtolower($phrase);
        $result = str_replace(array('/', '-', '+', '.', '&amp;', '&'), ' ', $result);
        $result = str_replace(array('ë', 'é', 'è'), 'e', $result);
        $result = preg_replace("/[^a-z0-9\s-]/", "", $result);
        $result = trim(preg_replace("/\s+/", " ", $result));
        $result = trim(substr($result, 0, 200));
        $result = preg_replace("/\s/", "-", $result);
        return $result;
    }

}
