<?php

// src/Dxs/CmsBundle/Controller/CategoryController.php

namespace Dxs\CmsBundle\Controller;

use Dxs\CmsBundle\Entity\CategoryEntity;
use Dxs\CmsBundle\Form\CategoryForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Dxs\CmsBundle\Custom\StringClass;

class CategoryController extends Controller {
    
    private $session;

    public function indexAction($action, $id, Request $request) {
        
        $this->session= $this->getRequest()->getSession();
        
        if (!$this->session->has('username')) {
            return $this->redirect($this->generateUrl('dxs_cms_login'));
        }

        switch ($action) {
            case 'list':
                return $this->listAction();
            case 'add':
                return $this->addAction($request);
            case 'edit':
                return $this->editAction($id, $request);
            case 'delete':
                return $this->deleteAction($id);
        }
    }
    
    public function listAction() {        
        
        $recordset = $this->getDoctrine()
                          ->getManager()
                          ->getRepository('DxsCmsBundle:CategoryEntity')
                          ->getCategories();

        return $this->render(
                    'DxsCmsBundle:Default:record.html.twig', array(
                    'recordset' => $recordset,
                    'page_title' => 'Categories',
                    'type' => 'categories',
                    'grid_titles' => array('ID', 'Categories', 'Slug', ''),
                    'username' => $this->session->get('username')    
                    )
        );
    }

    public function addAction($request) {

        $category = new CategoryEntity();
        $form = $this->createForm(new CategoryForm(0), $category, array(
            'attr' => array('class' => 'form-horizontal')
        ));

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            
                $string=new StringClass();
                $category->setSlug($category->getSlug()!==null ? $string->generateSlug($category->getSlug()) : $string->generateSlug($category->getCategories()));

                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $this->session->getFlashBag()->add('success', $category->getCategories() . ' successfully added to the database!');
                return $this->redirect($this->generateUrl('dxs_cms_categories', array('action' => 'list', 'id' => 0)));
        }

        return $this->render('DxsCmsBundle:Default:form.html.twig', array(
                    'form' => $form->createView(),
                    'page_title' => 'Add new category',
                    'type' => 'categories',
                    'username' => $this->session->get('username')
        ));
    }

    public function editAction($id, $request) {

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('DxsCmsBundle:CategoryEntity')->find($id);

        if (!$category){ 
            throw $this->createNotFoundException('No category found for id ' . $id);
        }

        $form = $this->createForm(new CategoryForm($id), $category, array(            
            'attr' => array('class' => 'form-horizontal')
        ));

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            
                $string=new StringClass();
                $category->setSlug($category->getSlug()!==null ? $string->generateSlug($category->getSlug()) : $string->generateSlug($category->getCategories()));

                $em->persist($category);
                $em->flush();                
                $this->session->getFlashBag()->add('success', $category->getCategories() . ' successfully edited!');
                return $this->redirect($this->generateUrl('dxs_cms_categories', array('action' => 'list', 'id' => 0)));           
        }

        return $this->render('DxsCmsBundle:Default:form.html.twig', array(
                    'form' => $form->createView(),
                    'page_title' => 'Edit category',
                    'type' => 'categories',
                    'id' => $id,
                    'username' => $this->session->get('username')
        ));
    }

    public function deleteAction($id) {

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('DxsCmsBundle:CategoryEntity')->find($id);

        if (!$category) {
            throw $this->createNotFoundException('No category found for id ' . $id);
        }

        $em->remove($category);
        $em->flush();

        $this->session->getFlashBag()->add('success', $category->getCategories() . ' deleted successfully!');
        return $this->redirect($this->generateUrl('dxs_cms_categories', array('action' => 'list', 'id' => 0)));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Dxs\CmsBundle\Entity\CategoryEntity',
        ));
    }
}
