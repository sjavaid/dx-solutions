<?php

// src/Dxs/CmsBundle/Controller/DefaultController.php

namespace Dxs\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller {

    private $session = null;

    public function indexAction() {

        if (!$this->session()->has('username')) {
            return $this->redirect($this->generateUrl('dxs_cms_login'));
        }
        
        /*$blog = $this->getDoctrine()
                ->getRepository('AdhlCmsBundle:BlogEntity')
                ->getLatestBlog();
        
        $comments = $this->getDoctrine()
                ->getRepository('AdhlCmsBundle:CommentEntity')
                ->getLatestComments();
*/
        return $this->render(
                    'DxsCmsBundle:Default:index.html.twig', array(
                    'username' => $this->session()->get('username'),
                          )
        );
    }

    public function session() {
        ($this->session === null ? $this->session = new session() : '');
        return $this->session;
    }

}
