<?php

// src/Dxs/CmsBundle/Controller/LoginController.php

namespace Dxs\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends Controller {

    public function indexAction(Request $request) {

        $session = new session();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('DxsCmsBundle:UserEntity');

        if ($request->getMethod() == 'POST') {

            $session->clear();
            $username = $request->get('username');
            $password = $request->get('password');
            $user = $repository->findOneBy(array('username' => $username, 'password' => $password));

            if ($user) {
                $session->set('username', $username);
                $session->set('user_id', $user->getId());
                return $this->redirect($this->generateUrl('dxs_cms_dashboard'));
            } else {
                return $this->render('DxsCmsBundle:Default:login.html.twig', array('error' => 'Login Error', 'username' => $username));
            }
        } else {
            if ($session->has('username')) {
                return $this->redirect($this->generateUrl('dxs_cms_dashboard'));
            }
            return $this->render('DxsCmsBundle:Default:login.html.twig', array('username' => ''));
        }
    }

    public function logoutAction(Request $request) {
        $session = $this->getRequest()->getSession();
        $session->clear();
        return $this->redirect($this->generateUrl('dxs_cms_login'));
    }

}