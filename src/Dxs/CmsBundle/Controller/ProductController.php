<?php

// src/Dxs/CmsBundle/Controller/ProductController.php

namespace Dxs\CmsBundle\Controller;

use Dxs\CmsBundle\Entity\ProductEntity;
use Dxs\CmsBundle\Form\ProductForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Dxs\CmsBundle\Custom\StringClass;

class ProductController extends Controller {
    
    private $session;

    public function indexAction($action, $id, Request $request) {
        
        $this->session= $this->getRequest()->getSession();
        
        if (!$this->session->has('username')) {
            return $this->redirect($this->generateUrl('dxs_cms_login'));
        }

        switch ($action) {
            case 'list':
                return $this->listAction();
            case 'add':
                return $this->addAction($request);
            case 'edit':
                return $this->editAction($id, $request);
            case 'delete':
                return $this->deleteAction($id);
        }
    }
    
    public function listAction() {

        $recordset = $this->getDoctrine()
                          ->getManager()
                          ->getRepository('DxsCmsBundle:ProductEntity')
                          ->getProducts();

        return $this->render(
                    'DxsCmsBundle:Default:record.html.twig', array(
                    'recordset' => $recordset,
                    'page_title' => 'Products',
                    'type' => 'products',
                    'grid_titles' => array('ID', 'Product', 'Price', 'Category', 'Slug', ''),
                    'username' => $this->session->get('username')    
                    )
        );
    }

    public function addAction($request) {

        $products = new ProductEntity();
        $form = $this->createForm(new ProductForm(0), $products, array(
            'attr' => array('class' => 'form-horizontal')
        ));

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            
                $string=new StringClass();
                $products->setSlug($products->getSlug()!==null ? $string->generateSlug($products->getSlug()) : $string->generateSlug($products->getProducts()));

                $em = $this->getDoctrine()->getManager();
                $em->persist($products);
                $em->flush();
                $this->session->getFlashBag()->add('success', $products->getProducts() . ' successfully added to the database!');
                return $this->redirect($this->generateUrl('dxs_cms_products', array('action' => 'list', 'id' => 0)));
        }

        return $this->render('DxsCmsBundle:Default:form.html.twig', array(
                    'form' => $form->createView(),
                    'page_title' => 'Add new product',
                    'type' => 'products',
                    'username' => $this->session->get('username')
        ));
    }

    public function editAction($id, $request) {

        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('DxsCmsBundle:ProductEntity')->find($id);

        if (!$products){ 
            throw $this->createNotFoundException('No product found for id ' . $id);
        }

        $form = $this->createForm(new ProductForm($id), $products, array(            
            'attr' => array('class' => 'form-horizontal')
        ));

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            
                $string=new StringClass();
                $products->setSlug($products->getSlug()!==null ? $string->generateSlug($products->getSlug()) : $string->generateSlug($products->getProducts()));

                $em->persist($products);
                $em->flush();                
                $this->session->getFlashBag()->add('success', $products->getProducts() . ' successfully edited!');
                return $this->redirect($this->generateUrl('dxs_cms_products', array('action' => 'edit', 'id' => $id)));           
        }

        return $this->render('DxsCmsBundle:Default:form.html.twig', array(
                    'form' => $form->createView(),
                    'page_title' => 'Edit Product',
                    'type' => 'products',
                    'id' => $id,
                    'username' => $this->session->get('username')
        ));
    }

    public function deleteAction($id) {

        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('DxsCmsBundle:ProductEntity')->find($id);

        if (!$products) {
            throw $this->createNotFoundException('No product found for id ' . $id);
        }

        $em->remove($products);
        $em->flush();

        $this->session->getFlashBag()->add('success', $products->getProducts() . ' deleted successfully!');
        return $this->redirect($this->generateUrl('dxs_cms_products', array('action' => 'list', 'id' => 0)));
    }
}
