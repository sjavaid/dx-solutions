<?php

// src/Dxs/CmsBundle/Entity/CategoryEntity.php

namespace Dxs\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Dxs\CmsBundle\Repository\CategoryRepository")
 * @ORM\Table(name="categories")
 */
class CategoryEntity {

    /**
     * @ORM\OneToMany(targetEntity="ProductEntity", mappedBy="category_id")
     * @ORM\OrderBy({"products" = "ASC"})
     */
    protected $products;

    public function __construct() {
        $this->products = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $categories;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;

    /**
     * Set id
     *
     * @param integer $id
     * @return CategoryEntity
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    public function __toString() {
        return $this->categories;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categories
     *
     * @param string $categories
     * @return CategoryEntity
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return string 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return CategoryEntity
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add products
     *
     * @param \Dxs\CmsBundle\Entity\ProductEntity $products
     * @return CategoryEntity
     */
    public function addProduct(\Dxs\CmsBundle\Entity\ProductEntity $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Dxs\CmsBundle\Entity\ProductEntity $products
     */
    public function removeProduct(\Dxs\CmsBundle\Entity\ProductEntity $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
