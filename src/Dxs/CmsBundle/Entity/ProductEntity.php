<?php

// src/Dxs/CmsBundle/Entity/ProductEntity.php

namespace Dxs\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Dxs\CmsBundle\Repository\ProductRepository")
 * @ORM\Table(name="products")
 */
class ProductEntity {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $products;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="Dxs\CmsBundle\Entity\CategoryEntity", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $category_id;

    /**
     * @ORM\Column(type="string", length=7000)
     */
    protected $details;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $slug;

    /**
     * Set id
     *
     * @param integer $id
     * @return ProductEntity
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    public function __toString() {
        return $this->products;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set products
     *
     * @param string $products
     * @return ProductEntity
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Get products
     *
     * @return string 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return ProductEntity
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return ProductEntity
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ProductEntity
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ProductEntity
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set category_id
     *
     * @param \Dxs\CmsBundle\Entity\CategoryEntity $categoryId
     * @return ProductEntity
     */
    public function setCategoryId(\Dxs\CmsBundle\Entity\CategoryEntity $categoryId = null)
    {
        $this->category_id = $categoryId;

        return $this;
    }

    /**
     * Get category_id
     *
     * @return \Dxs\CmsBundle\Entity\CategoryEntity 
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }
}
