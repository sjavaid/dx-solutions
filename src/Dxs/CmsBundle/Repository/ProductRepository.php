<?php

// src/Dxs/CmsBundle/Repository/ProductRepository.php

namespace Dxs\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ProductRepository
 */
class ProductRepository extends EntityRepository {

    public function getProducts() {

        return $this->getEntityManager()
                        ->createQuery(
                        '
                        SELECT p
                        FROM 
                        DxsCmsBundle:ProductEntity p
                        ORDER BY 
                        p.id DESC
                        '
                        )
                        ->getResult();
    }

    public function getProductByCategory($category) {

        return $this->getEntityManager()
                ->createQuery(
                        'SELECT p, c FROM DxsCmsBundle:ProductEntity p
                                JOIN p.category_id c
                                WHERE 
                                p.active = :active
                                AND
                                c.slug = :category
                        '
                )
                ->setParameter('active', 1)
                ->setParameter('category', $category)
                ->getResult();
    }

}
