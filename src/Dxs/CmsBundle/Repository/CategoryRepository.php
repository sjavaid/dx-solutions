<?php

// src/Dxs/CmsBundle/Repository/CategoryRepository.php

namespace Dxs\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CategoryRepository
 */
class CategoryRepository extends EntityRepository {

    public function getCategories() {

        return $this->getEntityManager()
                        ->createQuery(
                        '
                        SELECT c 
                        FROM 
                        DxsCmsBundle:CategoryEntity c 
                        ORDER BY 
                        c.categories ASC
                        '
                        )
                        ->getResult();
    }

    public function getSingleCategory($category) {

        $query = $this->getEntityManager()
                ->createQuery(
                        'SELECT p FROM DxsCmsBundle:CategoryEntity p
                            WHERE p.slug = :category
                        '        
                )
                ->setParameter('category', $category);


        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

}
