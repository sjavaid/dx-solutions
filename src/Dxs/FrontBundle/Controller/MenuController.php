<?php

// src/Dxs/FrontBundle/Controller/MenuController.php

namespace Dxs\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller {

    public function indexAction(Request $request) {
        
        $categories = $this->getDoctrine()
                ->getRepository('DxsCmsBundle:CategoryEntity')
                ->getCategories();

        return $this->render(
                    'DxsFrontBundle:Default:menu.html.twig', array(
                    'categories'=>$categories
                        )
        );
    }

}
