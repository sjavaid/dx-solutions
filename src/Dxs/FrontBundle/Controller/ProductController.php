<?php

// src/Dxs/FrontBundle/Controller/ProductController.php

namespace Dxs\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductController extends Controller {

    public function indexAction($category) {

        $em = $this->getDoctrine()->getManager();

        $products = $this->getDoctrine()
                ->getRepository('DxsCmsBundle:ProductEntity')
                ->getProductByCategory($category);

        return $this->render(
                    'DxsFrontBundle:Default:index.html.twig', array(
                    'page_title' => 'Products',
                    'products' => $products
                        )
        );
    }

}
