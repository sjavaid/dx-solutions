<?php

namespace DXS\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DxsFrontBundle:Default:index.html.twig', array('page_title' => 'Welcome'));
    }
}
